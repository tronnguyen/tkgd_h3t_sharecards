package sharecards.h3t.tkgd.sharecards;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {
    RecyclerView mRecyclerView;
    RecycleViewAdapter mRcvAdapter;
    List<String> data;
    private EditText editText;
    private Button imageButtonUpload;
    private ImageButton imageButton;
    private ImageButton imageButtonCancel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        mRecyclerView = findViewById(R.id.recycler_view);
        editText = findViewById(R.id.txt_search);
        editText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    RelativeLayout relativeLayout = findViewById(R.id.layout_temp);
                    relativeLayout.setVisibility(View.GONE);
                    // Perform action on key press
                    if(editText.getText().toString() != "" && editText.getText().toString() != null) {
                        data.add("ABC");
                        data.add("ABC");
                        data.add("ABC");
                        data.add("ABC");
                        data.add("ABC");
                        data.add("ABC");
                        data.add("ABC");
                        data.add("ABC");
                        mRcvAdapter.notifyDataSetChanged();
                    }
                    return true;
                }
                return false;
            }
        });
        imageButton = findViewById(R.id.btn_search);
        imageButtonUpload = findViewById(R.id.btn_upload);
        imageButtonCancel = findViewById(R.id.btn_cancel);
        data = new ArrayList<>();
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RelativeLayout relativeLayout = findViewById(R.id.layout_temp);
                relativeLayout.setVisibility(View.GONE);
                if(editText.getText().toString() != "" && editText.getText().toString() != null) {
                    data.add("ABC");
                    data.add("ABC");
                    data.add("ABC");
                    data.add("ABC");
                    data.add("ABC");
                    data.add("ABC");
                    data.add("ABC");
                    data.add("ABC");
                    mRcvAdapter.notifyDataSetChanged();
                }
            }
        });
        imageButtonUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        imageButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mRcvAdapter = new RecycleViewAdapter(data,this.getResources(),this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mRcvAdapter);
//        WebView webview = new WebView(this);
//        setContentView(webview);
//        webview.loadUrl("https://www.google.com.vn/imghp?hl=vi&tab=wi&authuser=0/");
    }
    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(SearchActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);
                } else if (items[item].equals("Choose from Library")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , 1);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }

            }
        });
        builder.show();
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case 0:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    //btnCircularImageView.setImageURI(selectedImage);
                    Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.image);

                    try {
                        bitmap =  MediaStore.Images.Media.getBitmap(this.getContentResolver(),selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(CreatePackActivity.ID == 0){
                        CreatePackActivity.avatar = bitmap;
                    }
                    else  if(CreatePackActivity.ID == 1){
                        CreatePackActivity.front = bitmap;
                    }
                    else  if(CreatePackActivity.ID == 2){
                        CreatePackActivity.back = bitmap;
                    }
                    onBackPressed();
                }

                break;
            case 1:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    //btnCircularImageView.setImageURI(selectedImage);
                    Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.image);

                    try {
                        bitmap =  MediaStore.Images.Media.getBitmap(this.getContentResolver(),selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(CreatePackActivity.ID == 0){
                        CreatePackActivity.avatar = bitmap;
                    }
                    else  if(CreatePackActivity.ID == 1){
                        CreatePackActivity.front = bitmap;
                    }
                    else  if(CreatePackActivity.ID == 2){
                        CreatePackActivity.back = bitmap;
                    }
                    onBackPressed();
                }
                break;
        }
    }
}
