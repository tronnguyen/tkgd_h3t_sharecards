package sharecards.h3t.tkgd.sharecards;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by Nguyen Van Tron on 5/6/2018.
 */

public class LearningActivity extends Activity implements FragmentManager.OnBackStackChangedListener {
    private int progressBarStatus = 5;
    private ProgressBar progressBar;
    private boolean mShowingBack = false;
    int []animalsRes = {R.drawable.dog,R.drawable.cat,R.drawable.hamster,R.drawable.chicken,R.drawable.bee,R.drawable.bird};
    String []animalsFront = {"Dog","Cat","Hamster","Chicken","Bee","Bird"};
    String []animalsBack = {"Con Chó","Con Mèo","Chuột Lang","Con Gà","Con Ong","Con Chim"};
    public static String textFront = "Dog";
    public static String textBack = "Con Chó";
    public static int resImageCard = R.drawable.dog;
    int index = 0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_learning);
        progressBar = findViewById(R.id.progress_bar);

        progressBar.setMax(40);
        progressBar.setProgress(progressBarStatus);
        ImageButton btnCancel = findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        final Button btnContinue = findViewById(R.id.btn_continue);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBarStatus = (progressBarStatus+5) % 40;
                //index = (index + 1) % 5;
                progressBar.setProgress(progressBarStatus);
//                textFront=animalsFront[index];
//                textBack=animalsBack[index];
//                resImageCard = animalsRes[index];
                btnContinue.setVisibility(View.GONE);
                flipCard();
            }
        });
        FrameLayout frameLayout = findViewById(R.id.container);
        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flipCard();
                btnContinue.setVisibility(View.VISIBLE);
            }
        });
        if (savedInstanceState == null) {
            getFragmentManager()
                    .beginTransaction()
                    .add(R.id.container, new CardFrontFragment())
                    .commit();
        }else {
            mShowingBack = (getFragmentManager().getBackStackEntryCount() > 0);
        }
        getFragmentManager().addOnBackStackChangedListener(this);
    }

    @Override
    public void onBackStackChanged() {
        mShowingBack = (getFragmentManager().getBackStackEntryCount() > 0);
        // When the back stack changes, invalidate the options menu (action bar).
        invalidateOptionsMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuItem item = menu.add(Menu.NONE, R.id.action_flip, Menu.NONE,
                mShowingBack ?
                        "Photo" :
                        "Info");
        item.setIcon(mShowingBack ?
                R.drawable.ic_launcher_background :
                R.drawable.ic_launcher_background);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Navigate "up" the demo structure to the launchpad activity.
                // See http://developer.android.com/design/patterns/navigation.html for more.
               // NavUtils.navigateUpTo(this, new Intent(this, LearningActivity.class));
                return true;
            case R.id.action_flip:
                //flipCard();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public static class CardFrontFragment extends Fragment {
        public CardFrontFragment() {}
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_card_front, container, false);
//            TextView textView = view.findViewById(R.id.txt_card);
//            ImageView imageView = view.findViewById(R.id.image_card);
//            textView.setText(textFront);
//            imageView.setImageResource(resImageCard);
            return view;
        }
    }
    public static class CardBackFragment extends Fragment {
        public CardBackFragment() {}
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_card_back, container, false);
//            TextView textView = view.findViewById(R.id.txt_card);
//            textView.setText(textBack);
            return view;
        }
    }
    private void flipCard() {
        if (mShowingBack) {
            getFragmentManager().popBackStack();
            return;
        }
        // Flip to the back.

        mShowingBack = true;
        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(
                        R.animator.right_in, R.animator.right_out,
                        R.animator.left_in, R.animator.left_out)
                .replace(R.id.container, new CardBackFragment())
                // Add this transaction to the back stack, allowing users to press Back
                // to get to the front of the card.
                .addToBackStack(null)
                // Commit the transaction.
                .commit();
    }
}
