package sharecards.h3t.tkgd.sharecards.home;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import sharecards.h3t.tkgd.sharecards.R;
import sharecards.h3t.tkgd.sharecards.RecycleViewAdapter;

/**
 * Created by Nguyen Van Tron on 5/29/2018.
 */

public class MyPageAdapter2 extends RecyclerView.Adapter<MyPageAdapter2.RecyclerViewHolder2>{

    private ArrayList<Integer> images;
    private ArrayList<String> names;
    private Activity context;
    private int type = 1;
    public void setType(int t){
        this.type = t;
    }
    public MyPageAdapter2(Activity context, ArrayList<Integer> images, ArrayList<String> names) {
        this.context = context;
        this.images=images;
        this.names=names;
    }




    @Override
    public RecyclerViewHolder2 onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = null;
         if(type==1){
             view  = inflater.inflate(R.layout.item2_slider, parent, false);
         }
         else if(type == 2){
             view  = inflater.inflate(R.layout.item_view_personal, parent, false);
         }

        return new RecyclerViewHolder2(view);
    }
    private int count = 0;
    @Override
    public void onBindViewHolder(MyPageAdapter2.RecyclerViewHolder2 holder, int position) {
        holder.imageView1.setImageResource(images.get(position));
        holder.textView.setText(names.get(position));
        if(type == 2) {
            count++;
            if (count % 2 == 0) {
                holder.button.setText("Public");
                holder.button.setBackgroundResource(R.drawable.blue_button);
            } else {
                holder.button.setText("Private");
                holder.button.setBackgroundResource(R.drawable.pink_button);
            }
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return images.size();
    }
    public class RecyclerViewHolder2 extends RecyclerView.ViewHolder {
        ImageView imageView1;
        TextView textView;
        Button button;
        public RecyclerViewHolder2(View itemView) {
            super(itemView);
            imageView1 =  itemView.findViewById(R.id.img_pack_avatar);
            textView =  itemView.findViewById(R.id.tv_pack_title);
            if(type == 2)
                button = itemView.findViewById(R.id.btn_public);


        }
    }
}