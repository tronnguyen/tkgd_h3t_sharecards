package sharecards.h3t.tkgd.sharecards;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

public class CreatePackActivity extends Activity {

    public static Bitmap avatar ;
    public static Bitmap front ;
    public static Bitmap back;
    public static int ID = 0;
    private ListView listview;
    private ArrayList<ModelCardView> itemlist = new ArrayList<ModelCardView>();
    private ImageView btnCircularImageView;
    private FloatingActionButton fab;
    private CardViewAdapter cardViewAdapter;
    String[] countryNames={"English(British)","English(American)"};
    int flags[] = {R.drawable.round_england, R.drawable.round_american};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_pack);
        avatar = BitmapFactory.decodeResource(getResources(), R.drawable.image);
        front = BitmapFactory.decodeResource(getResources(), R.drawable.image);
        back = BitmapFactory.decodeResource(getResources(), R.drawable.image);
        //Spinner
        Spinner spinner = (Spinner) findViewById(R.id.spinner_language);
        // Apply the adapter to the spinner
        CustomAdapter customAdapter=new CustomAdapter(getApplicationContext(),flags,countryNames);
        spinner.setAdapter(customAdapter);
        //Button
        final ImageButton btnBack = findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.temp_avatar);
        Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 30);

        btnCircularImageView = findViewById(R.id.imagePack);
        btnCircularImageView.setImageBitmap(bitmap);

        btnCircularImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(),SearchActivity.class);
                CreatePackActivity.ID = 0;
                startActivity(intent);
            }
        });

        itemlist.add(new ModelCardView(null,null,null,null));
        cardViewAdapter = new CardViewAdapter(this,itemlist);
        listview = findViewById(R.id.listviewCard);
        listview.setAdapter(cardViewAdapter);
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemlist.add(new ModelCardView());
                cardViewAdapter.notifyDataSetChanged();
                if(itemlist.size()>=2){
                    SaveDialog();
                    btnBack.setVisibility(View.GONE);
                    TextView textView = findViewById(R.id.tv_create);
                    textView.setVisibility(View.GONE);
                }
            }
        });

        cardViewAdapter.setListView(listview);
    }

    protected void SaveDialog(){
        RelativeLayout layout = findViewById(R.id.layout_auto_saved);
        layout.setVisibility(View.VISIBLE);
    }

    public static class ImageConverter {
        public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
            Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            final RectF rectF = new RectF(rect);
            final float roundPx = pixels;

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);

            return output;
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case 11:{
                    Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.image);
                    //Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 50);
                    btnCircularImageView.setImageBitmap(bitmap);
                break;
            }
            case 0:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    //btnCircularImageView.setImageURI(selectedImage);
                    Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.image);

                    try {
                        bitmap =  MediaStore.Images.Media.getBitmap(this.getContentResolver(),selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 100);
                    btnCircularImageView = null;
                    btnCircularImageView = findViewById(R.id.imagePack);
                    btnCircularImageView.setImageBitmap(bitmap);
                }

                break;
            case 1:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = (Uri) imageReturnedIntent.getExtras().get("uri"); //.getData();
                    //btnCircularImageView.setImageURI(selectedImage);
                    Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.image);

                    try {
                        bitmap =  MediaStore.Images.Media.getBitmap(this.getContentResolver(),selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 100);
                    btnCircularImageView = null;
                    btnCircularImageView = findViewById(R.id.imagePack);
                    btnCircularImageView.setImageBitmap(bitmap);
                }
                break;
            case 2:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    //btnCircularImageView.setImageURI(selectedImage);
                    Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.image);

                    try {
                        bitmap =  MediaStore.Images.Media.getBitmap(this.getContentResolver(),selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 100);
                    //cardViewAdapter.changeImage(circularBitmap);
                    updateView(cardViewAdapter.getPosition(),circularBitmap,requestCode);
                }
                break;
            case 3:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    //btnCircularImageView.setImageURI(selectedImage);
                    Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.image);

                    try {
                        bitmap =  MediaStore.Images.Media.getBitmap(this.getContentResolver(),selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 100);
                    //cardViewAdapter.changeImage(circularBitmap);
                    updateView(cardViewAdapter.getPosition(),circularBitmap,requestCode);
                }
                break;
        }
    }
    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library","Search From Google",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(CreatePackActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);
                } else if (items[item].equals("Choose from Library")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , 1);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
                else if(items[item].equals("Search From Google")){
                    //startActivity(new Intent(getBaseContext(),SearchActivity.class));
//                    Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.image);
//                    Bitmap circularBitmap = ImageConverter.getRoundedCornerBitmap(bitmap, 50);
//                    btnCircularImageView.setImageBitmap(circularBitmap);
                    startActivityForResult(new Intent(getBaseContext(),SearchActivity.class) , 11);
                }

            }
        });
        builder.show();
    }
    private void updateView(int index, Bitmap bitmap, int requestCode){
        View v = listview.getChildAt(index -
                listview.getFirstVisiblePosition());

        if(v == null)
            return;

        ImageView image = null;
        if(requestCode==1){
            image = v.findViewById(R.id.image_front);
            cardViewAdapter.getListItem().get(index).imageViewFront = image;
            cardViewAdapter.getListItem().get(index).imageViewFront.setImageBitmap(front);
            image.setVisibility(View.VISIBLE);
            image.setImageBitmap(front);
        } else if(requestCode == 2){
            image = v.findViewById(R.id.image_back);
            cardViewAdapter.getListItem().get(index).imageViewBack = image;
            cardViewAdapter.getListItem().get(index).imageViewBack.setImageBitmap(back);
            image.setVisibility(View.VISIBLE);
            image.setImageBitmap(back);
        }
        else if(requestCode == 0){
            btnCircularImageView.setImageBitmap(avatar);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        updateView(cardViewAdapter.getPosition(),null,CreatePackActivity.ID);
    }
}
