package sharecards.h3t.tkgd.sharecards;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.drm.DrmStore;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class CardViewAdapter extends ArrayAdapter {
    private Activity context;

    public ArrayList<ModelCardView> getListItem() {
        return listItem;
    }

    private ArrayList<ModelCardView> listItem = new ArrayList<>();
    private ImageView imageFront;
    private ImageView imageBack;
    private TextView textViewNumber;
    private ListView listView;
    public int getPosition() {
        return positionTemp;
    }

    private int positionTemp = 0;
    public CardViewAdapter(Activity context, ArrayList<ModelCardView> nameArrays) {
        super(context,R.layout.item_view,nameArrays);
        this.context = context;
        this.listItem = nameArrays;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable final View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        final View itemView = inflater.inflate(R.layout.item_view,null,true);
        positionTemp = position;
        final TextView txtFront = itemView.findViewById(R.id.txt_front);
        final TextView txtBack = itemView.findViewById(R.id.txt_back);
        final ImageButton btnAddPicFront = itemView.findViewById(R.id.btn_add_picture_front);
        final ImageButton btnAddTextFront = itemView.findViewById(R.id.btn_add_text_front);
        final ImageButton btnAddPicBack = itemView.findViewById(R.id.btn_add_picture_back);
        final ImageButton btnAddTextBack = itemView.findViewById(R.id.btn_add_text_back);

        imageFront = itemView.findViewById(R.id.image_front);
        imageBack = itemView.findViewById(R.id.image_back);
        textViewNumber = itemView.findViewById(R.id.card_number);
        textViewNumber.setText("Card " + (position + 1));
        final TextView txtWordFront = itemView.findViewById(R.id.txt_word_front);
        final TextView txtWordBack = itemView.findViewById(R.id.txt_word_back);

        RelativeLayout front = itemView.findViewById(R.id.layout_front);
        RelativeLayout back = itemView.findViewById(R.id.layout_back);
        ImageButton btnDeleteCard = itemView.findViewById(R.id.btn_delete_card);
        final ImageButton imageButtonCleanFront = itemView.findViewById(R.id.btn_clear_front);
        final ImageButton imageButtonCleanBack = itemView.findViewById(R.id.btn_clear_back);
        final TextView txtAddMoreFont = itemView.findViewById(R.id.txt_addmore_front);
        final TextView txtAddMoreBack = itemView.findViewById(R.id.txt_addmore_back);
        imageButtonCleanFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageFront.setVisibility(View.INVISIBLE);
                txtWordFront.setVisibility(View.VISIBLE);
                listItem.get(position).imageViewFront = null;
                listItem.get(position).textViewFront = null;
                notifyDataSetChanged();
            }
        });
        imageButtonCleanBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageBack.setVisibility(View.INVISIBLE);
                txtWordBack.setVisibility(View.VISIBLE);
                listItem.get(position).imageViewBack = null;
                listItem.get(position).textViewBack = null;
                notifyDataSetChanged();
            }
        });

        front.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtFront.setVisibility(View.INVISIBLE);
                btnAddPicFront.setVisibility(View.VISIBLE);
                btnAddTextFront.setVisibility(View.VISIBLE);
                imageButtonCleanFront.setVisibility(View.VISIBLE);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtBack.setVisibility(View.INVISIBLE);
                btnAddPicBack.setVisibility(View.VISIBLE);
                btnAddTextBack.setVisibility(View.VISIBLE);
                imageButtonCleanBack.setVisibility(View.VISIBLE);

            }
        });
        btnAddPicFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageFront.setVisibility(View.VISIBLE);
                //selectImageFront();
                Intent intent = new Intent(context,SearchActivity.class);
                CreatePackActivity.ID = 1;
                context.startActivity(intent);
            }
        });
        btnAddTextFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtWordFront.setVisibility(View.VISIBLE);
                listItem.get(position).textViewFront = txtWordFront;
                selectTextFront(itemView,4,position);

            }
        });
        btnAddPicBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageBack.setVisibility(View.VISIBLE);
                //selectImageBack();
                Intent intent = new Intent(context,SearchActivity.class);
                CreatePackActivity.ID = 2;
                context.startActivity(intent);
            }
        });
        btnAddTextBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtWordBack.setVisibility(View.VISIBLE);
                listItem.get(position).textViewBack = txtWordBack;
                selectTextFront(itemView,5,position);
            }
        });
        btnDeleteCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listItem.remove(position);
                notifyDataSetChanged();
            }
        });
        boolean flagFront = false;
        boolean flagBack = false;
        int i = 0, j = 0;
        if(listItem.get(position).imageViewFront!= null) {
            imageFront.setImageDrawable(listItem.get(position).imageViewFront.getDrawable());
            if(imageFront.getDrawable()!= null){
                imageFront.setVisibility(View.VISIBLE);
                flagFront = true;i=-1;
            }
        }
        if(listItem.get(position).imageViewBack!= null){
            imageBack.setImageDrawable(listItem.get(position).imageViewBack.getDrawable());
            if(imageBack.getDrawable()!= null){
                imageBack.setVisibility(View.VISIBLE);
                flagBack = true;j=-1;
            }
        }
        if(listItem.get(position).textViewFront != null){
            txtWordFront.setText(listItem.get(position).textViewFront.getText());
            if((txtWordFront.getText() != null) && (txtWordFront.getText().toString() != "")){
                txtWordFront.setVisibility(View.VISIBLE);
                txtFront.setVisibility(View.INVISIBLE);
                flagFront = true;i++;
            }
        }
        if(listItem.get(position).textViewBack != null){
            txtWordBack.setText(listItem.get(position).textViewBack.getText());
            if((txtWordBack.getText() != null) && (txtWordBack.getText().toString() != "")){
                txtWordBack.setVisibility(View.VISIBLE);
                txtBack.setVisibility(View.INVISIBLE);
                flagBack = true;j++;
            }
        }
        if(i==1){
            txtAddMoreFont.setVisibility(View.VISIBLE);
        }
        if(i==-1){
            txtAddMoreFont.setVisibility(View.GONE);
        }
        if(j==1){
            txtAddMoreBack.setVisibility(View.VISIBLE);
        }
        if(j==-1){
            txtAddMoreBack.setVisibility(View.GONE);
        }
        return  itemView;
    }

    private void selectImageFront() {
        final CharSequence[] items = { "Take Photo", "Choose from Library","Search From Google",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    context.startActivityForResult(takePicture, 2);
                } else if (items[item].equals("Choose from Library")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    context.startActivityForResult(pickPhoto , 2);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
                else if(items[item].equals("Search From Google")){
                    context.startActivity(new Intent(context,SearchActivity.class));
                }
            }
        });
        builder.show();
    }
    private void selectImageBack() {
        final CharSequence[] items = { "Take Photo", "Choose from Library","Search From Google",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    context.startActivityForResult(takePicture, 3);
                } else if (items[item].equals("Choose from Library")) {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    context.startActivityForResult(pickPhoto , 3);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
                else if(items[item].equals("Search From Google")){
                    context.startActivity(new Intent(context,SearchActivity.class));
                }
            }
        });
        builder.show();
    }
    private String selectTextFront(final View v, final int code, final int pos) {
        final CharSequence[] items = {"Done","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Add text for your flashcard");
        //builder.setMessage("Enter word");
        final EditText input = new EditText(context);
        input.setHeight(100);
        input.setWidth(340);
        input.setGravity(Gravity.LEFT);
        input.setInputType(EditorInfo.TYPE_TEXT_FLAG_MULTI_LINE);
        input.setImeOptions(EditorInfo.IME_ACTION_DONE);
        input.setHint("Enter word");
        builder.setView(input);

//        builder.setItems(items, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int item) {
//                if (items[item].equals("Done")) {
//                    updateView(v,pos,input.getText().toString(),code);
//                }
//                else if (items[item].equals("Cancel")) {
//                    dialog.dismiss();
//                }
//            }
//        });
        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                updateView(v,pos,input.getText().toString(),code);

            } });


        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            } });
        builder.show();
        return input.getText().toString();
    }
    private void updateView(View v, int index, String input, int requestCode){

        if(v == null)
            return;

        TextView textView = null;
        if(requestCode==4){
            textView =v.findViewById(R.id.txt_word_front);
            listItem.get(index).textViewFront = textView;
            listItem.get(index).textViewFront.setText(input);
        } else if(requestCode == 5){
            textView =v.findViewById(R.id.txt_word_back);
            listItem.get(index).textViewBack = textView;
            listItem.get(index).textViewBack.setText(input);
        }
        textView.setVisibility(View.VISIBLE);

        textView.setText(input);
    }

    public void setListView(ListView listView) {
        this.listView = listView;
    }
}
