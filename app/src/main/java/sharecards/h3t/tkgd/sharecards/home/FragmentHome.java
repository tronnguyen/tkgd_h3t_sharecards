package sharecards.h3t.tkgd.sharecards.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import sharecards.h3t.tkgd.sharecards.R;

/**
 * Created by Nguyen Van Tron on 6/1/2018.
 */

public class FragmentHome extends Fragment {

    private ViewPager viewPager;
    private RecyclerView mRecyclerView;
    private RecyclerView mRecyclerView2;
    private ArrayList<Integer> images;
    private ArrayList<String> names;
    private ArrayList<Integer> images_2;
    private ArrayList<String> names_2;
    private static int currentPage = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        names = new ArrayList<>();
        images = new ArrayList<>();
        names_2 = new ArrayList<>();
        images_2 = new ArrayList<>();
        names.add("Animals");
        names.add("Food");
        names.add("Emotion");
        images.add(R.drawable.dog);
        images.add(R.drawable.foods);
        images.add(R.drawable.compassion);
        names_2.add("Sport");
        names_2.add("Planets");
        names_2.add("Fruit");
        names_2.add("Jungle");
        names_2.add("Emotion");
        images_2.add(R.drawable.snow);
        images_2.add(R.drawable.desk);
        images_2.add(R.drawable.images);
        images_2.add(R.drawable.images_1);
        images_2.add(R.drawable.smile);
        names_2.add("Sport");
        names_2.add("Planets");
        names_2.add("Fruit");
        names_2.add("Jungle");
        names_2.add("Emotion");
        images_2.add(R.drawable.snow);
        images_2.add(R.drawable.desk);
        images_2.add(R.drawable.images);
        images_2.add(R.drawable.images_1);
        images_2.add(R.drawable.smile);

        ArrayList<Integer> imgPacks = new ArrayList<>();
        ArrayList<String> titlePacks = new ArrayList<>();
        imgPacks.add(R.drawable.download);
        imgPacks.add(R.drawable.foods);
        imgPacks.add(R.drawable.coctel__fresa__splash_256247);
        imgPacks.add(R.drawable.create);
        titlePacks.add("Animals");
        titlePacks.add("Vegetables");
        titlePacks.add("Drinks");
        titlePacks.add("Create");
        viewPager = rootView.findViewById(R.id.view_page);
        viewPager.setClipToPadding(false);
        viewPager.setPadding(40,0,40,0);
        viewPager.setAdapter(new MyPageAdapter(getActivity(),images,names));
        viewPager.setCurrentItem(0, true);

        LinearLayoutManager layoutManager= new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView =  rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(layoutManager);
        MyPageAdapter2 adapter2 = new MyPageAdapter2(getActivity(),images_2,names_2);
        adapter2.setType(1);
        mRecyclerView.setAdapter(adapter2);
        adapter2.notifyDataSetChanged();

//        mRecyclerView2=  rootView.findViewById(R.id.rv_packs);
//        mRecyclerView2.setLayoutManager(new GridLayoutManager(getContext(), 2));
//        MyPageAdapter2 adapter3 = new MyPageAdapter2(getActivity(),imgPacks,titlePacks);
//        adapter3.setType(3);
//        mRecyclerView2.setAdapter(adapter3);
//        adapter3.notifyDataSetChanged();
        // Auto start of viewpager
//        final Handler handler = new Handler();
//        final Runnable Update = new Runnable() {
//            public void run() {
//                if (currentPage == images.size()) {
//                    currentPage = 0;
//                }
//                viewPager.setCurrentItem(currentPage++, true);
//            }
//        };
//        Timer swipeTimer = new Timer();
//        swipeTimer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                handler.post(Update);
//            }
//        }, 2500, 2500);
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public FragmentHome(){

    }
}
