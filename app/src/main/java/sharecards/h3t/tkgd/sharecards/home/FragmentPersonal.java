package sharecards.h3t.tkgd.sharecards.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;

import sharecards.h3t.tkgd.sharecards.CreatePackActivity;
import sharecards.h3t.tkgd.sharecards.R;

/**
 * Created by Nguyen Van Tron on 6/1/2018.
 */

public class FragmentPersonal extends Fragment {
    private RecyclerView mRecyclerView;
    private ArrayList<Integer> images;
    private ArrayList<String> names;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View rootView = inflater.inflate(R.layout.fragment_personal, container, false);
        LinearLayoutManager layoutManager= new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL, false);
        mRecyclerView =  rootView.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(layoutManager);
        names = new ArrayList<>();
        images = new ArrayList<>();
        names.add("Animals");
        names.add("Foods");
        names.add("Emotion");
        images.add(R.drawable.dog);
        images.add(R.drawable.foods);
        images.add(R.drawable.compassion);
        names.add("Sport");
        names.add("Planets");
        names.add("Fruit");
        names.add("Jungle");
        names.add("Emotion");
        images.add(R.drawable.snow);
        images.add(R.drawable.desk);
        images.add(R.drawable.images);
        images.add(R.drawable.images_1);
        images.add(R.drawable.smile);
        MyPageAdapter2 adapter2 = new MyPageAdapter2(getActivity(),images,names);
        adapter2.setType(2);
        mRecyclerView.setAdapter(adapter2);
        adapter2.notifyDataSetChanged();
        Button createBtn = rootView.findViewById(R.id.btn_create_pack);
        createBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), CreatePackActivity.class));
            }
        });
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public FragmentPersonal(){

    }
}
