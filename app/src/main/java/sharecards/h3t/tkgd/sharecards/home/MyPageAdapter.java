package sharecards.h3t.tkgd.sharecards.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import sharecards.h3t.tkgd.sharecards.LearningActivity;
import sharecards.h3t.tkgd.sharecards.PackDetailActivity;
import sharecards.h3t.tkgd.sharecards.R;

/**
 * Created by Nguyen Van Tron on 5/29/2018.
 */

public class MyPageAdapter extends PagerAdapter {

    private ArrayList<Integer> images;
    private ArrayList<String> names;
    private LayoutInflater inflater;
    private Activity context;

    public MyPageAdapter(Activity context, ArrayList<Integer> images, ArrayList<String> names) {
        this.context = context;
        this.images=images;
        this.names=names;
        inflater = context.getLayoutInflater();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.item_slider, view, false);
        ImageView myImage =  myImageLayout
                .findViewById(R.id.img_pack_avatar);

        myImage.setImageResource(images.get(position));
        TextView name = myImageLayout.findViewById(R.id.tv_pack_title);
        name.setText(names.get(position));
        Button btnStart = myImageLayout.findViewById(R.id.btn_pack_learn);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, PackDetailActivity.class));
            }
        });
        view.addView(myImageLayout);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}