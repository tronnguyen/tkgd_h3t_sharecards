package sharecards.h3t.tkgd.sharecards;

import android.widget.ImageView;
import android.widget.TextView;

public class ModelCardView {
    ImageView imageViewFront;
    TextView textViewFront;
    ImageView imageViewBack;
    TextView textViewBack;
    ModelCardView(ImageView imageF, TextView textF,
    ImageView imageB, TextView textB){
        imageViewFront = imageF;
        textViewFront = textF;
        imageViewBack = imageB;
        textViewBack = textB;
    }

    public ModelCardView() {

    }
}
