package sharecards.h3t.tkgd.sharecards;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nguyen Van Tron on 5/6/2018.
 */

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.RecyclerViewHolder>{

    private List<String> data = new ArrayList<>();
    private Resources resource;
    private Activity activity;
    public RecycleViewAdapter(List<String> data, Resources res, Activity activity) {
        this.data = data;
        this.resource = res;
        this.activity = activity;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.itemsearch, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        Bitmap bitmap = BitmapFactory.decodeResource(resource, R.drawable.image);
        Bitmap circularBitmap = CreatePackActivity.ImageConverter.getRoundedCornerBitmap(bitmap, 50);
        holder.imageView1.setImageBitmap(circularBitmap);
        holder.imageView2.setImageBitmap(circularBitmap);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView1;
        ImageView imageView2;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            imageView1 =  itemView.findViewById(R.id.image);
            imageView2 =  itemView.findViewById(R.id.image2);
            imageView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   activity.onBackPressed();
                }
            });
            imageView1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.onBackPressed();
                }
            });
        }
    }
}